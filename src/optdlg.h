/***************************************************************************
 *   crwx-ng                                                               *
 *   Copyright (C) 2007-2009 Vadim Lopatin <coolreader.org@gmail.com>      *
 *   Copyright (C) 2024 Aleksey Chernov <valexlin@gmail.com>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA 02110-1301, USA.                                                   *
 ***************************************************************************/

// Options dialog
#ifndef OPTIONS_DLG_H_INCLUDED
#define OPTIONS_DLG_H_INCLUDED

#include <wx/treebook.h>
#include <wx/dialog.h>

#include <crprops.h>

enum
{
    ID_OPTIONS_TREEBOOK,
    ID_OPTIONS_WINDOW,
    ID_OPTIONS_WINDOW_MENU,
    ID_OPTIONS_WINDOW_STATUSBAR,
    ID_OPTIONS_WINDOW_TOOLBAR,
    ID_OPTIONS_PAGE_TITLE,
};

class wxStaticBoxSizer;
class wxCheckBox;
class wxComboBox;
class wxSpinCtrl;

class PropOption
{
protected:
    const char* _option;
    wxWindow* _control;
public:
    PropOption(wxWindow* control, const char* option) : _option(option), _control(control) { }
    virtual ~PropOption() { }
    virtual void ControlToOption(CRPropRef props) = 0;
    virtual void OptionToControl(CRPropRef props) = 0;
    virtual int getActionId() {
        return 0;
    }
    const char* getOption() const {
        return _option;
    }
    wxWindow* getControl() const {
        return _control;
    }
    virtual wxString getControlValue() const = 0;
    virtual void onAction() { }
};

class OptPanel: public wxPanel
{
protected:
    wxStaticBoxSizer* _sizer;
    LVPtrVector<PropOption> _opts;
    wxWindow* AddControl(wxWindow* control);
    wxCheckBox* AddCheckbox(const char* option, wxString caption, bool defValue);
    wxComboBox* AddCombobox(const char* option, wxString caption, const wxArrayString& option_titles,
                            const wxArrayString& option_values, const wxString& defValue);
    wxComboBox* AddFontFaceCombobox(const char* option, wxString caption);
    wxControl* AddColor(const char* option, wxString caption, lvColor defValue, int buttonId);
    wxSpinCtrl* AddNumberBox(const char* option, wxString caption, int defValue, int min, int max);
    virtual void CreateControls() = 0;
    virtual void OnOptionControlChanged(const wxString& option, const wxString& controlValue) { }
    // event handlers
    void OnButtonClicked(wxCommandEvent& event);
    void OnComboBoxItemSelected(wxCommandEvent& event);
    void OnCheckBoxClicked(wxCommandEvent& event);
    void OnSpinCtrlUpdated(wxSpinEvent& event);
public:
    OptPanel();
    void Create(wxWindow* parent, wxWindowID id, wxString title);
    virtual void PropsToControls(CRPropRef props);
    virtual void ControlsToProps(CRPropRef props);
protected:
    DECLARE_EVENT_TABLE()
};

class CR3OptionsDialog: public wxDialog
{
private:
    wxTreebook* _notebook;
    OptPanel* _opt_window;
    OptPanel* _opt_page;
    OptPanel* _opt_view;
    OptPanel* _opt_font;
    OptPanel* _opt_lang;
    OptPanel* _opt_app;
    CRPropRef _props;
    CRPropRef _oldprops;
public:
    CRPropRef getNewProps() {
        return _props;
    }
    CRPropRef getOldProps() {
        return _oldprops;
    }
    CRPropRef getChangedProps() {
        return _oldprops ^ _props;
    }
    virtual void PropsToControls();
    virtual void ControlsToProps();
    CR3OptionsDialog(CRPropRef props);
    virtual ~CR3OptionsDialog();
    bool Create(wxWindow* parent, wxWindowID id);
};

#define OPTION_DIALOG_BUTTON_START 2000
#define OPTION_DIALOG_BUTTON_END   2099

#endif // OPTIONS_DLG_H_INCLUDED
