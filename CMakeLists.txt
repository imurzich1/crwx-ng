project(crwx-ng)

cmake_minimum_required(VERSION 3.12 FATAL_ERROR)

#set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/modules/")

#  VERSION_DEV_A/CODE description:
# alphaX - 0X; beta X - 1X; rcX - 2X; releaseX - 3X (or empty)
set(VERSION_MAJOR 0)
set(VERSION_MINOR 3)
set(VERSION_PATCH 1)
set(VERSION_DEV_A "")
set(VERSION_DEV_CODE 30)
set(VERSION "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")
if (VERSION_DEV_A)
  set(VERSION "${VERSION}-${VERSION_DEV_A}")
endif()
set(PACKAGE_VERSION ${VERSION})
# for windows rc file
set(PRODUCTVERSION_V "${VERSION_MAJOR},${VERSION_MINOR},${VERSION_PATCH},${VERSION_DEV_CODE}")
set(FILEVERSION_V ${PRODUCTVERSION_V})

set(PACKAGE_DATADIR	${CMAKE_INSTALL_PREFIX}/share)

option(ADD_DEBUG_EXTRA_OPTS "Add extra debug flags and technique" OFF)

configure_file(config.h.cmake ${CMAKE_BINARY_DIR}/config.h)

# wxWidgets
find_package(wxWidgets 3.2 REQUIRED base core html)
include(${wxWidgets_USE_FILE})
include_directories( ${wxWidgets_INCLUDE_DIRS} )

# crengine-ng
find_package(crengine-ng REQUIRED)

include_directories(${CRENGINE_NG_INCLUDE_DIR})

# Add include path for config.h
include_directories(${CMAKE_BINARY_DIR})

# force use exceptions
set(CMAKE_CXX_FLAGS         "${CMAKE_CXX_FLAGS} -fexceptions")
# add debug defines
set(CMAKE_C_FLAGS_DEBUG     "${CMAKE_C_FLAGS_DEBUG}     -D_DEBUG")
set(CMAKE_CXX_FLAGS_DEBUG   "${CMAKE_CXX_FLAGS_DEBUG}   -D_DEBUG")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -DNDEBUG")

if(ADD_DEBUG_EXTRA_OPTS)
	add_definitions(-D_GLIBCXX_DEBUG -D_GLIBCXX_DEBUG_PEDANTIC)
	set(DEBUG_EXTRA_FLAGS "-fstack-check -fstack-protector -fno-omit-frame-pointer -Wshift-overflow=2")
	if ("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
		set (DEBUG_EXTRA_FLAGS "${DEBUG_EXTRA_FLAGS} -fsanitize=address -fsanitize=undefined -fno-sanitize-recover -fno-sanitize=alignment")
		set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -fsanitize=address -fsanitize=undefined -fno-sanitize-recover -fstack-check -fstack-protector")
		set(CMAKE_SHARED_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -fsanitize=address -fsanitize=undefined -fno-sanitize-recover -fstack-check -fstack-protector")
	endif ("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${DEBUG_EXTRA_FLAGS}")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${DEBUG_EXTRA_FLAGS}")
endif(ADD_DEBUG_EXTRA_OPTS)

if(UNIX)
  add_definitions(-DCRUI_DATA_DIR="${CMAKE_INSTALL_PREFIX}/share/crwx/")
else()
  add_definitions(-DCRUI_DATA_DIR="")
endif()

if (WIN32)
	set(CMAKE_RC_COMPILER windres)
	# set rc syntax
	set(CMAKE_RC_COMPILE_OBJECT "<CMAKE_RC_COMPILER> <FLAGS> <DEFINES> -O coff -o <OBJECT> -i <SOURCE>")
	set(CMAKE_RC_SOURCE_FILE_EXTENSIONS rc)
	list(JOIN wxWidgets_INCLUDE_DIRS " -I" wxWidgets_INCLUDE_DIRS_FLAGS)
	set(CMAKE_RC_FLAGS "-I${CMAKE_BINARY_DIR} -I${wxWidgets_INCLUDE_DIRS_FLAGS}")
	# enable resource language for this project
	enable_language(RC)
endif(WIN32)

add_subdirectory(src)
